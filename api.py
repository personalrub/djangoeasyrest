from django.urls import path, include
from VHouse import views;
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.apps import apps
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
import json

app_name = ""
tables_with_token = []
token_column_name = "token"

models_creation_black_list = []
models_update_black_list = []
models_get_black_list = []
models_all_black_list = []

@csrf_exempt
def serialize_to_json(models):
    js = json.loads(serializers.serialize("json", models))
    array = []
    for element in js:
        element['fields']['pk'] = element['pk']
        array.append(element['fields'])
    return json.dumps(array)

@csrf_exempt
def all(request, model):
    if model in models_all_black_list:
        return HttpResponse(status=403)
    modelObj = apps.get_model(app_label=app_name, model_name=model)
    if model in tables_with_token:
        dict = {}
        dict[token_column_name] = request.GET[token_column_name]
        models = modelObj.objects.filter(**dict)
    else:
        models = modelObj.objects.all()
    return HttpResponse(serialize_to_json(models))

@csrf_exempt
def getById(request, model, value):
    return get(request, model, None, value)

@csrf_exempt
def new(request, model):
    if model in models_creation_black_list:
        return HttpResponse(status=403)
    filter = {}
    for key in request.POST:
        filter[key] = request.POST[key]
    if model in tables_with_token:
        filter[token_column_name] = request.POST[token_column_name]
    model = apps.get_model(app_label=app_name, model_name=model)
    modelInstance = model.objects.create(**filter)
    modelInstance.save()
    return HttpResponse(serialize_to_json(model.objects.filter(pk=modelInstance.pk)))

@csrf_exempt
def get(request, model, attribute, value):
    if model in models_get_black_list:
        return HttpResponse(status=403)
    if attribute == None:
        attribute = "pk"
    filter = {}
    filter[attribute] = value
    if model in tables_with_token:
        filter[token_column_name] = request.GET[token_column_name]
    return HttpResponse(serialize_to_json(apps.get_model(app_label=app_name, model_name=model).objects.filter(**filter)))

@csrf_exempt
def putById(request, model, value, attribute):
    return put(request, model, "pk", value, attribute)

@csrf_exempt
def put(request, model, attribute, value, attributeSet):
    if model in models_update_black_list:
        return HttpResponse(status=403)
    if attribute == None:
        attribute = "pk"
    valueSet = request.POST.get("value")
    filter = {}
    filter[attribute] = value
    if model in tables_with_token:
        filter[token_column_name] = request.POST[token_column_name]
    models = apps.get_model(app_label=app_name, model_name=model).objects.filter(**filter)
    if models and len(models)>0:
        setattr(models[0], attributeSet, valueSet)
        models[0].save()
    return HttpResponse(serialize_to_json(models))

urlpatterns = [
    path('<model>/', all),

    path('<model>/new/', new), #Must be POST

    path('<model>/<int:value>/', getById),

    path('<model>/<int:value>/<attribute>/', putById), #Must be POST

    path('<model>/<attribute>/<value>/', get),

    path('<model>/<attribute>/<value>/<attributeSet>/', put) #Must be POST
]
